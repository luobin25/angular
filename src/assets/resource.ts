import {User} from '../model/user';

export class Resource {
  // isFolder: boolean;
  size: number;
  // modifiedDate: string;
  mime_type: string;
  location: string;
  original_name: string;
  private: boolean;
  owner: User;
  directory: String;
}
