import {Component, OnInit} from '@angular/core';
import {NavigationStart, Router} from '@angular/router';
import {AuthenticationService} from '../../service/authentication.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'app';
  isLogged = false;

  constructor(private router: Router, private authService: AuthenticationService) { }

  ngOnInit() {
    this.router.events.subscribe( event => {
      if ( event instanceof NavigationStart ) {
        this.isLogged = this.authService.isLogged();
      }
    });
  }
  logout() {
    this.authService.logout();
  }
}
