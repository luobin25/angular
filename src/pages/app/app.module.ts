import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import { AppComponent } from './app.component';

import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatToolbarModule} from '@angular/material/toolbar';
import {AboutComponent} from '../about/about.component';
import {RouterModule, Routes} from '@angular/router';
import {NotFoundComponent} from '../notFound/notFound.component';
import {MainComponent} from '../main/main.component';
import {AccountComponent} from '../account/account.component';
import {LoginComponent} from '../login/login.component';
import {RegisterComponent } from '../register/register.component';
import {AuthGuard} from '../../guards/auth.guard';
import {AuthenticationService} from '../../service/authentication.service';

import {HeaderComponent} from '../../components/header/header.component';
import {SidebarComponent} from '../../components/sidebar/sidebar.component';
import {ContentComponent, VolumeConverterPipe} from '../../components/content/content.component';
import {SaveDlComponent} from '../../components/dialog/save-dl/save-dl.component';
import {RenameDlComponent} from '../../components/dialog/rename-dl/rename-dl.component';
import {UploadDlComponent} from '../../components/dialog/upload-dl/upload-dl.component';
import {NewfolderDlComponent} from '../../components/dialog/newfolder-dl/newfolder-dl.component';

import { MatIconModule, MatInputModule, MatButtonModule, MatMenuModule, MatSidenavModule, MatProgressBarModule} from '@angular/material';
import { MatFormFieldModule, MatListModule, MatDividerModule, MatTooltipModule, MatDialogModule} from '@angular/material';
import {MatTabsModule, MatRadioModule} from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {UserService} from '../../service/user.service';
import {JwtInterceptor} from '../../helpers/jwt.interceptor';
import {IndexComponent} from '../index/index.component';
import {FileService} from '../../service/file.service';
import {WarnDlComponent} from '../../components/dialog/warn-dl/warn-dl.component';


const appRoutes: Routes = [
  {path: 'index', component: IndexComponent},
  { path: 'about', component: AboutComponent },
  { path: 'main', component: MainComponent, canActivate: [AuthGuard] },
  { path: 'main/:dirPath', component: MainComponent, canActivate: [AuthGuard]},
  // { path: 'main', component: MainComponent, canActivate: [AuthGuard], children: [
  //     { path: '**', component: MainComponent, canActivate: [AuthGuard]}
  //   ] },
  { path: 'account', component: AccountComponent },
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent},
  { path: '',   redirectTo: 'index', pathMatch: 'full' },
  { path: '**', component: NotFoundComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    IndexComponent,
    AboutComponent,
    NotFoundComponent,
    MainComponent,
    AccountComponent,
    LoginComponent,
    RegisterComponent,
    HeaderComponent,
    SidebarComponent,
    ContentComponent,
    SaveDlComponent,
    RenameDlComponent,
    UploadDlComponent,
    NewfolderDlComponent,
    WarnDlComponent,
    VolumeConverterPipe
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    MatInputModule,
    MatIconModule,
    MatButtonModule,
    MatMenuModule,
    MatSidenavModule,
    MatProgressBarModule,
    MatFormFieldModule,
    MatRadioModule,
    MatListModule,
    MatTooltipModule,
    MatDividerModule,
    MatDialogModule,
    MatTabsModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [
    AuthGuard,
    AuthenticationService,
    UserService,
    FileService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: JwtInterceptor,
      multi: true
    },
  ],
  entryComponents: [
    SaveDlComponent,
    RenameDlComponent,
    UploadDlComponent,
    NewfolderDlComponent,
    WarnDlComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
