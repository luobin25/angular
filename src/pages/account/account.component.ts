import {Component, OnInit} from '@angular/core';
import {UserService} from '../../service/user.service';
import {AuthenticationService} from '../../service/authentication.service';
import {User} from '../../model/user';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.scss']
})
export class AccountComponent implements OnInit {

  passWordHideType = true;
  LoginFormGroup: FormGroup;

  constructor(
              private router: Router,
              private authenticationService: AuthenticationService) {
  }

  ngOnInit() {

  }

  editUserInfo() {

  }
}
