import { Component, OnInit } from '@angular/core';



@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss']
})
export class IndexComponent implements OnInit {
  background = '../../assets/img/background1.jpg';
  choices = [
    '../../assets/img/background1.jpg',
    '../../assets/img/background2.jpg',
    '../../assets/img/background3.jpg'
  ];
  constructor() {}

  ngOnInit() {
  }


}
