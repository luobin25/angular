import { Component } from '@angular/core';
import { FormGroup, FormBuilder, Validators} from '@angular/forms';
import {UserService} from '../../service/user.service';
import {User} from '../../model/user';
import {Router} from '@angular/router';
import {AuthenticationService} from '../../service/authentication.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})

export class RegisterComponent {
  RegisterFormGroup: FormGroup;
  passWordHideType = true;

  constructor(private formBuilder: FormBuilder,
              private userService: UserService,
              private router: Router,
              private authenticationService: AuthenticationService) {
    this.RegisterFormGroup = this.formBuilder.group({
      nameControl: ['guest'],
      emailControl: ['', [Validators.required, Validators.email] ],
      passControl: ['', [Validators.required, Validators.minLength(6)]]
    });
  }

  onSubmit() {
    const username = this.RegisterFormGroup.get('nameControl').value;
    const email = this.RegisterFormGroup.get('emailControl').value;
    const password = this.RegisterFormGroup.get('passControl').value;
    const user = {  username, email, password};
    this.userService.create(<User>user).subscribe(
      (resp: any) => {
        if (resp.success = 'true') {
          this.authenticationService.setLoggedUser(resp.data);
          this.router.navigateByUrl('/main');
        }
      }
    );
  }
}
