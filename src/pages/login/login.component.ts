import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {AuthenticationService} from '../../service/authentication.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit{
  passWordHideType = true;
  LoginFormGroup: FormGroup;

  constructor(private formBuilder: FormBuilder,
              private router: Router,
              private authenticationService: AuthenticationService) {
    this.LoginFormGroup = this.formBuilder.group({
      emailControl: ['', [Validators.required, Validators.email] ],
      passControl: ['', Validators.required]
    });
  }

  ngOnInit() {
    this.authenticationService.logout();
  }

  onSubmit() {
    const login = this.LoginFormGroup.get('emailControl').value;
    const passWord = this.LoginFormGroup.get('passControl').value;
    this.authenticationService.login(login, passWord).subscribe(
      (response: any) => {
        console.log(response);
        if (response.success === true) {
          this.authenticationService.setLoggedUser(response['message']);
          this.router.navigateByUrl(`/main`);
        }else {
          console.log(`hello`);
        }
      }
    );
  }
}

