import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { environment } from '../environments/environment';
import 'rxjs/add/operator/map';

@Injectable()
export class AuthenticationService {
  constructor(private http: HttpClient) { }

  login(email: string, password: string) {
    /*return this.http.post<any>(environment.api_url + '/login', { password: password, email: email })
      .map(user => {
        // login successful if there's a jwt token in the response
        console.log('hello');
        console.log(user);
        if (user && user.token) {
          // store user details and jwt token in local storage to keep user logged in between page refreshes
          localStorage.setItem('currentUser', JSON.stringify(user));
        }

        return user;
      });*/
    return this.http.post(environment.api_url + '/login', { email: email, password: password });
  }

  setLoggedUser(user: any): void {
    console.log(user);
    localStorage.setItem(`currentUser`, JSON.stringify(user));
    if (user.token) {
      localStorage.setItem(`token`, JSON.stringify(user.token));
    }
  }

  getLoggedUser(): any {
    return JSON.parse(localStorage.getItem('currentUser'));
  }

  isLogged(): boolean {
    return localStorage.getItem('currentUser') !== null && localStorage.getItem('token') !== null;
  }

  logout() {
    localStorage.clear();
  }
}
