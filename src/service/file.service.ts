import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {environment} from '../environments/environment';

@Injectable()
export class  FileService {
  constructor(private http: HttpClient) {}

  getFilesByUserId(id: number) {
    const  url = `${environment.api_url}/file/${id}`;
    return this.http.get(url);
  }

  getSpecLoctionFilesByUserId(id: number, dirPath: string) {
    const  url = `${environment.api_url}/file/${id}`;
    return this.http.get(url, {
      params: new HttpParams().set('dirPath', dirPath)
    });
  }

  getSpecTypeFilesByUserId(id: number, fileType: string) {
    const  url = `${environment.api_url}/file/${id}/filter`;
    return this.http.get(url, {
      params: new HttpParams().set('fileType', fileType)
    });
  }

  getSpecSearchFilesByUserId(id: number, searchWord: string) {
    const  url = `${environment.api_url}/file/${id}/search`;
    return this.http.post(url, { searchWord: searchWord });
  }

  uploadFile(id: number, file, dirPath: string) {
    const  url = `${environment.api_url}/file/${id}`;
    // 因为考虑到api中storage是先创建文件夹再将file内容转换的，所以我们只能用params代替body
   return this.http.post(url,  file,
    {
      params: new HttpParams().set('dirPath', dirPath)
    });
  }

  addFolder(id: number, foldName: string, dirPath: string) {
    const url = `${environment.api_url}/file/${id}/addFolder`;
    // const params = new HttpParams();
    // params.set('foldName', foldName);
    // params.set('directory', directory);
    // this.http.get(url, { params: params});
    return this.http.post(url, { foldName: foldName, dirPath: dirPath });
  }

  renameFile(fileId: number, newName: string) {
    const url = `${environment.api_url}/file/${fileId}/rename`;
    return this.http.post(url, { newName: newName });
  }

  downloadFile(fileId: number) {
    const url = `${environment.api_url}/file/${fileId}/download`;
    return this.http.get(url, {
      responseType: 'blob'
    });
  }

  deleteFile(fileId: number) {
    const url = `${environment.api_url}/file/${fileId}`;
    return this.http.delete(url);
  }

  shareFile(fileId: number) {
    const url = `${environment.api_url}/file/${fileId}/share`;
    return this.http.get(url);
  }

  saveFile(fileId: number, dirPath: string) {
    const  url = `${environment.api_url}/file/${fileId}/save`;
    return this.http.get(url,
      {
        params: new HttpParams().set('dirPath', dirPath)
      });
  }
}
