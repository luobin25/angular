import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

import { User } from '../model/user';
import { environment } from '../environments/environment';

@Injectable()
export class UserService {

  constructor(private http: HttpClient) {}

  create(user: User) {
    return this.http.post(environment.api_url + '/signup', user);
  }

  getUserById(id: number) {
    const url = `${environment.api_url}/user/${id}`;
    return this.http.get<User>(url);
  }

  editUserById(id: number, user: User) {
    const url = `${environment.api_url}/user/${id}`;
    return this.http.post(url, user);
  }
}
