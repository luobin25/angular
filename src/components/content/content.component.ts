import { Component, OnInit } from '@angular/core';
import {Resource} from '../../assets/resource';
import { Location } from '@angular/common';
import { Pipe, PipeTransform } from '@angular/core';
import {switchMap} from 'rxjs/operator/switchMap';
// import {RESOURCES} from '../../assets/mock-resources';

import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import {SaveDlComponent} from '../dialog/save-dl/save-dl.component';
import {UploadDlComponent} from '../dialog/upload-dl/upload-dl.component';
import {NewfolderDlComponent} from '../dialog/newfolder-dl/newfolder-dl.component';
import {RenameDlComponent} from '../dialog/rename-dl/rename-dl.component';
import {FileService} from '../../service/file.service';
import {AuthenticationService} from '../../service';
import {ActivatedRoute} from '@angular/router';
import {DomSanitizer} from '@angular/platform-browser';
import {WarnDlComponent} from '../dialog/warn-dl/warn-dl.component';

@Component({
  selector: 'app-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.scss']
})
export class ContentComponent implements OnInit {

  resources: Resource[];
  userId: number;
  dirPath: string;
  selectFile: any;
  searchWord: string;

  constructor(public dialog: MatDialog, private route: ActivatedRoute, private location: Location,
              private fileService: FileService, private authWS: AuthenticationService) {}

  ngOnInit() {
    // const dirPath = this.route.snapshot.paramMap.get('dirPath');
    this.route.params.subscribe((params) => {
      const fileType = this.route.snapshot.queryParams['fileType'];
      this.userId = this.authWS.getLoggedUser()._id;
      if (fileType) {
        this.fileService.getSpecTypeFilesByUserId(this.userId, fileType).subscribe(
          (response: any) => {
            if (response.success === true) {
              this.resources = response.message;
              console.log(response.message);
            }
          });
      }else {
        if (params.dirPath) {
          this.dirPath = params.dirPath + '/';
        }else {
          this.dirPath = '';
        }
        console.log('dirpath: ' + this.dirPath);
        this.fileService.getSpecLoctionFilesByUserId(this.userId, this.dirPath).subscribe(
          (response: any) => {
            if (response.success === true) {
              this.resources = response.message;
              console.log(response.message);
            }
          });
      }
    });

  }

  onSelectFile(file) {
    this.selectFile = file;
    console.log('fileId' + this.selectFile);
  }

  downloadFile() {
    this.fileService.downloadFile(this.selectFile._id).subscribe(
      (data: any) => {
        console.log(data);
        const blob = new Blob([data], {type: 'pplication/vnd.openxmlformats-officedocument.spreadsheetml.sheet'});
        const objectUrl = URL.createObjectURL(blob);
        const a = document.createElement('a');
        document.body.appendChild(a);
        a.setAttribute('style', 'display:none');
        a.setAttribute('href', objectUrl);
        a.setAttribute('download', this.selectFile.original_name);
        a.click();
        URL.revokeObjectURL(objectUrl);
      }
    );
  }

  deleteFile() {
    this.fileService.deleteFile(this.selectFile._id).subscribe(
      (response: any) => {
        if (response.success === true) {
           const index = this.resources.indexOf(this.selectFile);
           this.resources.splice(index, 1);
        }
      }
    );
  }

  shareFile() {
    this.fileService.shareFile(this.selectFile._id).subscribe(
      (response: any) => {
        if (response.success === true) {
          this.dialog.open(WarnDlComponent, {
            data: { message: this.selectFile._id, Info: 'Share to your friends' },
            width: '400px',
            height: '250px'
          });
        }else {
          this.dialog.open(WarnDlComponent, {
            data: { message: response.message, Info: 'Error' },
            width: '400px',
            height: '250px'
          });
        }
      }
    );
  }

  search() {
    if (this.searchWord !== '' ) {
      this.fileService.getSpecSearchFilesByUserId(this.userId, this.searchWord).subscribe(
        (response: any) => {
          if (response.success === true) {
            this.resources = response.message;
          }
        });
    }
  }
  goBack() {
    this.location.back();
  }

  openSaveDialog() {
    this.dialog.open(SaveDlComponent, {
      data: { dirPath: this.dirPath, resources: this.resources },
      width: '400px',
      height: '250px'
    });
  }
  openUploadDialog() {
    const dialogRef2 = this.dialog.open(UploadDlComponent, {
      data: { userId: this.userId, dirPath: this.dirPath, resources: this.resources },
      width: '400px',
      height: '250px'
    });
  }
  openNewFolderDialog() {
    this.dialog.open(NewfolderDlComponent,  {
      data: { userId: this.userId, dirPath: this.dirPath, resources: this.resources },
      width: '400px',
      height: '250px'
    });
  }

  openRenameDialog() {
    this.dialog.open(RenameDlComponent, {
      data: { selectFile: this.selectFile, resources: this.resources },
      width: '400px',
      height: '250px'
    });
  }
}

@Pipe({name: 'volumeConverter'})
export class VolumeConverterPipe implements PipeTransform {
  transform(value: number, ...args: any[]): string {
    if (value >= Math.pow(1024, 3)) {
      return value / Math.pow(1024, 3) + 'G';
    } else if (value >= Math.pow(1024, 2) && value <= Math.pow(1024, 3)) {
      return value / Math.pow(1024, 2) + 'M';
    } else if (value >= 1024 && value <= Math.pow(1024, 2)) {
      return value / 1024 + 'K';
    } else {
      return value + 'B';
    }
  }
}

