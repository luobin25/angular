import { Component, OnInit } from '@angular/core';
import {AuthenticationService} from '../../service';
import {NavigationStart, Router} from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  isLogged = false;
  currentUser;

  constructor(private router: Router, private authWS: AuthenticationService) { }

  ngOnInit() {
    this.isLogged = this.authWS.isLogged();
    if (this.isLogged) {
      this.currentUser = this.authWS.getLoggedUser();
    }
  }

  logout() {
    this.authWS.logout();
    this.router.navigateByUrl(`/index`);
  }
}
