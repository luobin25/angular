import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

@Component({
  selector: 'app-warn-dl',
  templateUrl: './warn-dl.component.html',
  styleUrls: ['./warn-dl.component.scss']
})
export class WarnDlComponent implements OnInit {

  constructor(@Inject(MAT_DIALOG_DATA) private data: any) { }

  ngOnInit() {
    console.log(this.data);
  }

}
