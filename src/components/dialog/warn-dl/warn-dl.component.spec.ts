import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WarnDlComponent } from './warn-dl.component';

describe('WarnDlComponent', () => {
  let component: WarnDlComponent;
  let fixture: ComponentFixture<WarnDlComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WarnDlComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WarnDlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
