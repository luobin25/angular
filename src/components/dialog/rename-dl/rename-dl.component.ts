import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';
import {FileService} from '../../../service/file.service';

@Component({
  selector: 'app-rename-dl',
  templateUrl: './rename-dl.component.html',
  styleUrls: ['./rename-dl.component.scss']
})
export class RenameDlComponent implements OnInit {

  newFileName: string;
  constructor(@Inject(MAT_DIALOG_DATA) private data: any, private fileService: FileService) { }

  ngOnInit() {
  }
  rename() {
    if (this.newFileName !== '') {
      this.fileService.renameFile(this.data.selectFile._id, this.newFileName).subscribe(
        (data: any) => {
          this.data.selectFile.original_name = this.newFileName;
          this.data.selectFile = null;
        });
    }
  }
}
