import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RenameDlComponent } from './rename-dl.component';

describe('RenameDlComponent', () => {
  let component: RenameDlComponent;
  let fixture: ComponentFixture<RenameDlComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RenameDlComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RenameDlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
