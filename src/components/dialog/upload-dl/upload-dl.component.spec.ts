import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UploadDlComponent } from './upload-dl.component';

describe('UploadDlComponent', () => {
  let component: UploadDlComponent;
  let fixture: ComponentFixture<UploadDlComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UploadDlComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UploadDlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
