import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {FileService} from '../../../service/file.service';
import {AuthenticationService} from '../../../service';
import {HttpHeaders} from '@angular/common/http';
import {MAT_DIALOG_DATA} from '@angular/material';

@Component({
  selector: 'app-upload-dl',
  templateUrl: './upload-dl.component.html',
  styleUrls: ['./upload-dl.component.scss']
})
export class UploadDlComponent implements OnInit {
  @ViewChild('fileInput') fileInput;


  constructor(@Inject(MAT_DIALOG_DATA) private data: any, private fileService: FileService) { }

  ngOnInit() {
  }

  upload() {
    const fileBrowser = this.fileInput.nativeElement;
    if (fileBrowser.files && fileBrowser.files[0]) {
      const file = fileBrowser.files[0];
      const fileData = new FormData();
      fileData.append('file', file);
      // fileData.append('dirPath', this.data.dirPath);
      this.fileService.uploadFile(this.data.userId, fileData, this.data.dirPath).subscribe(
        (data: any) => {
          console.log(this.data.resources);
          this.data.resources.splice(0, 0, data.message);
          console.log(this.data.resources);
        });
    }
  }
}
