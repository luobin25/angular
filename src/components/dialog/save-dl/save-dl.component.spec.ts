import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SaveDlComponent } from './save-dl.component';

describe('SaveDlComponent', () => {
  let component: SaveDlComponent;
  let fixture: ComponentFixture<SaveDlComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SaveDlComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SaveDlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
