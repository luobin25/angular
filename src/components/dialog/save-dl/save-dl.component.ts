import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';
import {FileService} from '../../../service/file.service';

@Component({
  selector: 'app-save-dl',
  templateUrl: './save-dl.component.html',
  styleUrls: ['./save-dl.component.scss']
})
export class SaveDlComponent implements OnInit {

  saveFileId: number;
  constructor(@Inject(MAT_DIALOG_DATA) private data: any, private fileService: FileService) { }

  ngOnInit() {
  }

  saveFile() {
    if (this.saveFileId) {
      this.fileService.saveFile(this.saveFileId, this.data.dirPath).subscribe(
        (data: any) => {
          this.data.resources.splice(0, 0, data.message);
        });
    }
  }
}
