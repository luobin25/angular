import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewfolderDlComponent } from './newfolder-dl.component';

describe('NewfolderDlComponent', () => {
  let component: NewfolderDlComponent;
  let fixture: ComponentFixture<NewfolderDlComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewfolderDlComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewfolderDlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
