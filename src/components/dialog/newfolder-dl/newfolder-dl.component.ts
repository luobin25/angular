import {Component, Inject, Input, OnInit} from '@angular/core';
import {FileService} from '../../../service/file.service';
import {AuthenticationService} from '../../../service';
import {MAT_DIALOG_DATA} from '@angular/material';

@Component({
  selector: 'app-newfolder-dl',
  templateUrl: './newfolder-dl.component.html',
  styleUrls: ['./newfolder-dl.component.scss']
})
export class NewfolderDlComponent implements OnInit {

  foldName: string;
  constructor(@Inject(MAT_DIALOG_DATA) private data: any, private fileService: FileService) {
  }

  ngOnInit() {
  }

  newFolder() {
    if (this.foldName !== '') {
      this.fileService.addFolder(this.data.userId, this.foldName, this.data.dirPath).subscribe(
        (data: any) => {
          console.log(this.data.resources);
          this.data.resources.splice(0, 0, data.message);
          console.log(this.data.resources);
        });
    }
  }
}
